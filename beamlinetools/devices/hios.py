import os
import numpy as np

from ophyd import Component as Cpt, FormattedComponent as FCpt
from ophyd import EpicsSignal, Device

from ophyd import PseudoSingle, PseudoPositioner
from ophyd.pseudopos import pseudo_position_argument,real_position_argument

from bessyii_devices.axes import AxisTypeB

class AxysTypeBOptics(AxisTypeB):
    stop_signal     = Cpt(EpicsSignal,  '_AKTION', kind='omitted')

    def __init__(self, prefix, **kwargs):
        super().__init__(prefix, **kwargs)
        self.readback.name = self.name



class PseudoSingleOptics(PseudoSingle):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.readback.name = self.name


class HIOSPseudo(PseudoPositioner):

    rhm2 = FCpt(AxysTypeBOptics, 'd011pgm1:{self._ch_name}', labels={"hios"}) 
    hm2  = Cpt(PseudoSingleOptics, labels={"hios"})
    
    def __init__(self, ch_name='PH_2',**kwargs):
        self._ch_name = ch_name
        super().__init__(**kwargs)
    
    def _get_hios_file_path(self):
        # Get the current script's directory (where the script is saved)
        script_dir = os.path.dirname(os.path.realpath(__file__))
        # Get the parent directory of the script's directory
        parent_dir = os.path.abspath(os.path.join(script_dir, os.pardir))
        path_to_hios_folder = os.path.join(parent_dir, 'hios')
        path_to_offset_file = os.path.join(path_to_hios_folder, 'offset.txt')
        return path_to_offset_file
    
    def read_offset(self):
        path_to_offset_file = self._get_hios_file_path()
        try:
            with open(path_to_offset_file, 'r') as file:
                offset = float(file.read().strip())
        except ValueError:
            # If the file exists but does not contain a valid number
            print("Error: The file does not contain a valid number. I set the offset to 0.88.",
                    "\nTo set a custom offset use hios.save_new_offset(your_offset)")
            offset = 0.88
            with open(path_to_offset_file, 'w') as file:
                file.write(str(offset) + '\n')
        except Exception as e:
            # Handle other unexpected exceptions
            raise Exception(f"An unexpected error occurred: {e}")
        return offset
    
    def save_new_offset(self, new_offset):
        path_to_offset_file = self._get_hios_file_path()
        print(f"{path_to_offset_file}, {new_offset}")
        with open(path_to_offset_file, 'w') as file:
            file.write(str(new_offset) + '\n')  # Adding a newline for clarity


    @pseudo_position_argument
    def forward(self, pseudo_pos):
        '''Run a forward (pseudo -> real) calculation'''
        offset = self.read_offset()
        return self.RealPosition(rhm2=pseudo_pos.hm2+offset)

    @real_position_argument
    def inverse(self, real_pos):
        '''Run an inverse (real -> pseudo) calculation'''
        offset = self.read_offset()
        return self.PseudoPosition(hm2=real_pos.rhm2+offset)



class HIOS(Device):
    hm1 = Cpt(AxysTypeBOptics, 'PH_1', name='rhm1', labels={"hios"}) 
    hm2pseudo  = Cpt(HIOSPseudo, name='rhm2')
    htr  = Cpt(AxysTypeBOptics, 'PH_3', name='rhtr', labels={"hios"}) 

    def read_offset(self):
        offset = self.hm2pseudo.read_offset()
        return offset
    
    def save_new_offset(self, new_offset):
        self.hm2pseudo.save_new_offset(new_offset)
