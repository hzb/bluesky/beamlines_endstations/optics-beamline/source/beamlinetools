
from ophyd import Component as Cpt
from ophyd import EpicsSignal, EpicsMotor
from bessyii_devices.axes import AxisTypeD
from ophyd import Device



class FSU(Device):
    slt1 = Cpt(EpicsMotor, 'fsus1', labels={"fsu"})
    flt1 = Cpt(EpicsMotor, 'fsuf1', labels={"fsu"})
    slt2 = Cpt(EpicsMotor, 'fsus2', labels={"fsu"}) 
    flt2 = Cpt(EpicsMotor, 'fsuf2', labels={"fsu"}) 