from bessyii_devices.keithley import Keithley6517, Keithley6514
from ophyd.status import DeviceStatus, SubscriptionStatus
from ophyd import Signal, Component as Cpt, EpicsSignal
from bessyii_devices.positioners import InternalSignal
import time

class OpticsKeithley6514(Keithley6514):

    # Make a parameter that can be set to increase the number of readings triggered per
    # reading recorded. For continuous measurements this should be set to 1 and the device
    # should be in fixed range mode. For stepwise measurements using auto-ranging
    # this value should be set to 2 so that the erroneous value after an auto-range is 
    # ignored.
    trigger_rep = Cpt(Signal,value=2, kind='config')
    _trigger_counter = Cpt(Signal,value=0, kind='omitted')

    def unstage(self):

        # I found that if the device was sent a read command too quickly at the end of the measurement
        # then the keithley would get upset and send back garbage. Adding a 1 second sleep here is 
        # ok because the measurement has already finished (we are in unstage method)
        time.sleep(1)
        self.scan.put('.1 second')
        super().unstage()  

    def trigger(self):
           
        # Create a callback which will be called when the device responds with a value
        # Optionally repeatedly trigger trigger_rep times to avoid keithley bad value 
        # after autoranging. 
        def new_value(*,old_value,value,**kwargs):          

            # If we have done all the required tiggers
            if self._trigger_counter.get() == self.trigger_rep.get():

                #Reset the trigger counter for next acq
                self._trigger_counter.put(1)

                # Clear the subscription.
                self.readback.clear_sub(new_value)

                # Set Finished
                status.set_finished()

            # If we still have triggers to issue
            else:
                
                # Increment the trigger counter
                self._trigger_counter.put( self._trigger_counter.get() + 1)

                # Issue the trigger
                self.trigger_cmd.put(1)
            

        #Create the status object
        status = DeviceStatus(self.readback,timeout = 10.0)

        #Connect the callback that will set finished and clear sub
        self.readback.subscribe(new_value,event_type=Signal.SUB_VALUE,run=False)
        
        # Issue the first trigger to kick everything off
        self.trigger_cmd.put(1)   
        
        return status



class OpticsKeithley6517(OpticsKeithley6514):

       
    vsrc_ena            = Cpt(EpicsSignal, 'cmdVoltSrcEna', kind="omitted")
    vsrc                = Cpt(EpicsSignal, 'rbkVoltSrc' , write_pv='setVoltSrc',       kind='config')
    trig_mode    		= Cpt(EpicsSignal, 'rbkTrigCont', write_pv='setTrigCont',        string='True',      kind='config')    #single or continuous mode. Bypasses event detection (trig_src)

    def unstage(self):
        # we don't need to add the sleep here, it is already in 6514 parent class
        self.trig_mode.put("Continuous")
        super().unstage()  

