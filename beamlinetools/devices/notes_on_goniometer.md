# Notes on Goniometer IOC and Ophyd Device

In spec the goniometer IOC is exposing a setpoint and a status PV. It does the following:

```
set setpoint value
wait (0.5 seconds)
read the status PV 
if status == 3591:
    wait(some time)
    finished
else:
    wait(0.5 seconds)

```

In Bluesky we are doing this:

```
add offset to setpoint value
set setpoint+offset
wait for |readback - setpoint|< atol AND status =3591 (using a callback to monitor for changes)
wait (settle time)
```

By using the PVPositionerComparator we get around the problem of the status PV not toggling, this is handled by the class. We only have to make the condition work.

We set the busy record to 0 because?

