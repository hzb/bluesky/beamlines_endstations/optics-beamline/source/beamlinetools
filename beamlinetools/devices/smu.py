from ophyd import Component as Cpt
from ophyd import EpicsMotor
from bessyii_devices.m3_m4_mirrors import SMUUE52SGM


class M1SWITCH(SMUUE52SGM):

    switch = Cpt(EpicsMotor, "M1", name='switch')
