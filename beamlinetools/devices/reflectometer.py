import csv
import time

from ophyd import Component as Cpt
from ophyd import FormattedComponent as FCpt
from ophyd import EpicsSignal, EpicsSignalRO, Device, PVPositioner

from ophyd import (Component as Cpt)
from beamlinetools.utils.file_helper import import_csv, update_low_limit, update_high_limit

from bessyii_devices.positioners import PVPositionerComparator



class ReflectometerAxis(PVPositioner):

    setpoint     = FCpt(EpicsSignal,  '{self.prefix}UserSetPoint{self._ch_name}',   kind='normal')
    readback     = FCpt(EpicsSignalRO,'{self.prefix}UserSetPointRB{self._ch_name}', kind='hinted')
    offset       = FCpt(EpicsSignal,  '{self.prefix}Offset{self._ch_name}',         kind='normal')
    abs_readback = FCpt(EpicsSignalRO,'{self.prefix}PositionRB{self._ch_name}',     kind='normal')
    done     = FCpt(EpicsSignal,  '{self.prefix}ActualBusy{self._ch_name}',     kind='omitted')

    done_value = 0 # This is the value of the status when the motors are in position
    stop_signal = FCpt(EpicsSignal,  'Reflect:stopTripod', kind='omitted')

    limits_file_path = '/opt/bluesky/beamlinetools/beamlinetools/reflectometer_calibration/absolute_limits.csv'
    
    def describe(self):
        ret = super().describe()
        ret[self.readback.name]['precision'] = 4
        return ret

    @property
    def absolute_low_limit(self):
        return self._absolute_low_limit

    @property
    def absolute_high_limit(self):
        return self._absolute_high_limit
    
    @absolute_low_limit.setter
    def absolute_low_limit(self, value):
        self._absolute_low_limit = value
        update_low_limit(self.limits_file_path, self.name, self._absolute_low_limit)

    @absolute_high_limit.setter
    def absolute_high_limit(self, value):
        self._absolute_high_limit = value
        update_high_limit(self.limits_file_path, self.name, self._absolute_high_limit)

    def __init__(self, prefix, ch_name=None, rb_name=None, atol=0.01,**kwargs):
        self._ch_name = ch_name
        self._rb_name = rb_name
        super().__init__(prefix, **kwargs)
        self.readback.name = self.name
        self.atol = atol

        df = import_csv(self.limits_file_path)
        self.absolute_low_limit = df[self.name][0]
        self.absolute_high_limit = df[self.name][1]


    def stop(self, success=False):
        # This is to avoid tripod to go in undefined state 1216
        # because `move` calles `stop` at the end to ensure that 
        # motors are stopped. Tripod is a soft-motor, this logic
        # is not working
        if self.done.get() != self.done_value:
            super().stop(success=success)
        
        

    def set_to_zero(self):
        """Set the user position to zero by updating the offset accordingly
        """
        self.set_current_position(0)

    def set_current_position(self, value):
        """Set the user position to a value by updating the offset accordingly

        Args:
            value (int, float): the new readback value
        """        
        if (type(value) is float) or (type(value) is int):
            new_offset = value - self.abs_readback.get()
            self.offset.put(new_offset)
            time.sleep(.5)
            print(f'Offset: {self.offset.get()}')
            print(f"{self.name} is set to: {self.readback.get()}")
        else :
            raise ValueError(f"Value type is not allowed. Valid types are float or int, you are using {type(value)}.")
    
    def move(self, position, wait=True, timeout=None, moved_cb=None):
        # check if in limits, otherwise abort
        absolute_position = position - self.offset.get()
        if self.absolute_low_limit <= absolute_position <= self.absolute_high_limit:
            return super().move(position, moved_cb=moved_cb, timeout=timeout)
        else:
            raise ValueError(f"Movement not allowed. Absolute position will be outside limits! Current limits: {self.absolute_low_limit}, {self.absolute_high_limit} - Selected position : {absolute_position}")




class GoniometerAxis(PVPositionerComparator):

    setpoint     = FCpt(EpicsSignal,  '{self.prefix}UserSetPoint{self._ch_name}',   kind='normal')
    readback     = FCpt(EpicsSignalRO,'{self.prefix}UserSetPointRB{self._ch_name}', kind='hinted')
    offset       = FCpt(EpicsSignal,  '{self.prefix}Offset{self._ch_name}',         kind='normal')
    abs_readback = FCpt(EpicsSignalRO,'{self.prefix}PositionRB{self._ch_name}',     kind='normal')
    _rb_value    = FCpt(EpicsSignalRO,  'Reflect:{self._rb_name}{self._ch_name}', kind='omitted')
    stop_signal  = FCpt(EpicsSignal,  'Reflect:stopGoniometer', kind='omitted')
    
    status_pv       = FCpt(EpicsSignalRO,  'Reflect:getGTReady',     kind='omitted')
    status_value = 3591 # This is the value of the status when the motors are in position
    
    busy_ioc     = FCpt(EpicsSignal,  '{self.prefix}ActualBusy{self._ch_name}',     kind='omitted')

    limits_file_path = '/opt/bluesky/beamlinetools/beamlinetools/reflectometer_calibration/absolute_limits.csv'
    
    def describe(self):
        ret = super().describe()
        ret[self.readback.name]['precision'] = 4
        return ret

    @property
    def absolute_low_limit(self):
        return self._absolute_low_limit

    @property
    def absolute_high_limit(self):
        return self._absolute_high_limit
    
    @absolute_low_limit.setter
    def absolute_low_limit(self, value):
        self._absolute_low_limit = value
        update_low_limit(self.limits_file_path, self.name, self._absolute_low_limit)

    @absolute_high_limit.setter
    def absolute_high_limit(self, value):
        self._absolute_high_limit = value
        update_high_limit(self.limits_file_path, self.name, self._absolute_high_limit)

    def __init__(self, prefix, ch_name=None, rb_name=None, atol=0.01,**kwargs):
        self._ch_name = ch_name
        self._rb_name = rb_name
        super().__init__(prefix, **kwargs)
        self.readback.name = self.name
        self.atol = atol

        df = import_csv(self.limits_file_path)
        self.absolute_low_limit = df[self.name][0]
        self.absolute_high_limit = df[self.name][1]


    def stop(self, success=False):
        # This is to avoid tripod to go in undefined state 1216
        # because `move` calles `stop` at the end to ensure that 
        # motors are stopped. Tripod is a soft-motor, this logic
        # is not working
        if self.busy_ioc.get() != self.done_value:
            super().stop(success=success)
        

    def set_to_zero(self):
        """Set the user position to zero by updating the offset accordingly
        """
        self.set_current_position(0)

    def set_current_position(self, value):
        """Set the user position to a value by updating the offset accordingly

        Args:
            value (int, float): the new readback value
        """        
        if (type(value) is float) or (type(value) is int):
            new_offset = value - self.abs_readback.get()
            self.offset.put(new_offset)
            time.sleep(.5)
            print(f'Offset: {self.offset.get()}')
            print(f"{self.name} is set to: {self.readback.get()}")
        else :
            raise ValueError(f"Value type is not allowed. Valid types are float or int, you are using {type(value)}.")
    
    def move(self, position, wait=True, timeout=None, moved_cb=None):
        # check if in limits, otherwise abort
        absolute_position = position - self.offset.get()
        if self.absolute_low_limit <= absolute_position <= self.absolute_high_limit:
            return super().move(position, moved_cb=moved_cb, timeout=timeout)
        else:
            raise ValueError(f"Movement not allowed. Absolute position will be outside limits! Current limits: {self.absolute_low_limit}, {self.absolute_high_limit} - Selected position : {absolute_position}")
    
    def done_comparator(self, readback, setpoint):
        # check if readback value is inside valid range
        has_arrived = setpoint-self.atol < readback < setpoint+self.atol
        # return false as long as we detect movement
        if not has_arrived:
            return False
        else: 
            if (self.status_pv.get() == self.status_value) and (self.busy_ioc.get() == 1): 
                self.busy_ioc.put(0)
            return True



class DetectorAxis(GoniometerAxis):

    _rb_value    = FCpt(EpicsSignalRO,  'Reflect:{self._rb_name}{self._ch_name}', kind='omitted')
    stop_signal  = FCpt(EpicsSignal,  'Reflect:stopDet', kind='omitted')
    status_pv       = FCpt(EpicsSignal,  'Reflect:Status',     kind='omitted')
    status_value = 1279 # This is the value of the status when the motors are in position
    

class OpticsReflectometer(Device):
    # Tripod
    tx = Cpt(ReflectometerAxis, '', ch_name='TX', atol=0.001, labels={"reflectometer"})
    ty = Cpt(ReflectometerAxis, '', ch_name='TY', atol=0.001, labels={"reflectometer"})
    tz = Cpt(ReflectometerAxis, '', ch_name='TZ', atol=0.001, labels={"reflectometer"})
    rx = Cpt(ReflectometerAxis, '', ch_name='RX', atol=0.001, labels={"reflectometer"})
    ry = Cpt(ReflectometerAxis, '', ch_name='RY', atol=0.001, labels={"reflectometer"})
    rz = Cpt(ReflectometerAxis, '', ch_name='RZ', atol=0.001, labels={"reflectometer"})

    # Detector
    det1 = Cpt(DetectorAxis, '', ch_name='Det1', rb_name="Get",  atol=0.001, labels={"reflectometer"})
    det2 = Cpt(DetectorAxis, '', ch_name='Det2', rb_name="Get",  atol=0.001, labels={"reflectometer"})
    
    # Goniometer
    phi = Cpt(GoniometerAxis, '', ch_name='G0A', rb_name="get", atol=0.005, labels={"reflectometer"})
    tha = Cpt(GoniometerAxis, '', ch_name='G1T', rb_name="get", atol=0.005, labels={"reflectometer"})
    twt = Cpt(GoniometerAxis, '', ch_name='G2T', rb_name="get", atol=0.005, labels={"reflectometer"})

    read_attrs       = ['tx.readback', 'ty.readback', 'tz.readback', 
                        'rx.readback', 'ry.readback', 'rx.readback',
                        'phi.readback', 'tha.readback', 'twt.readback',
                        'det1.readback', 'det2.readback']


    def _get_data_dict(self):
        self._data_dict = {}
        with open('/opt/bluesky/beamlinetools/beamlinetools/reflectometer_calibration/calibration_data.csv', 'r') as csvfile:
            reader = csv.reader(csvfile, delimiter=',', quotechar='"')
            for row in reader:
                axis, val = row
                self._data_dict[axis] = float(val)
        return self._data_dict
    
    def initialise_axes(self):

        data_dict = self._get_data_dict()
        self.tx.setpoint.set(data_dict['tx'])
        self.ty.setpoint.set(data_dict['ty'])
        self.tz.setpoint.set(data_dict['tz'])
        self.rx.setpoint.set(data_dict['rx'])
        self.ry.setpoint.set(data_dict['ry'])
        self.rz.setpoint.set(data_dict['rz'])

    def set_to_zero(self):
        self.tx.set_to_zero()
        self.ty.set_to_zero()
        self.tz.set_to_zero()
        self.rx.set_to_zero()
        self.ry.set_to_zero()
        self.rz.set_to_zero()
        self.tha.set_to_zero()
        self.twt.set_to_zero()
        self.phi.set_to_zero()

    def set_gon_to_zero(self):
        self.tha.set_to_zero()
        self.twt.set_to_zero()
        self.phi.set_to_zero()
    
    def unblock_reflectometer(self):
        """
        Reset the reflctometer in case of status errors that freeze it.

        It moves relative the axes tx +1 and then it moves it back 
        to the original positiion
        """
        current_pos = self.tx.readback.get()
        self.tx.setpoint.set(current_pos+0.001)
        time.sleep(5)
        self.tx.setpoint.set(current_pos-0.001)



