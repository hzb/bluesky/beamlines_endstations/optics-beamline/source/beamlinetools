import pandas as pd

def import_csv(file_path):
    """
    Import a CSV file into a pandas DataFrame.

    Args:
        file_path (str): The path to the CSV file.

    Returns:
        pandas.DataFrame: The DataFrame containing the CSV data.
    """
    try:
        df = pd.read_csv(file_path)
        return df
    except FileNotFoundError:
        print(f"File '{file_path}' not found.")
        return None
    except Exception as e:
        print(f"Error occurred while importing CSV file: {e}")
        return None
    
def update_value(file_path, header, limit_index, limit_value):
    df = import_csv(file_path)
    df.loc[limit_index, header] = limit_value
    df.to_csv(file_path, index=False)

def update_low_limit(file_path, header, limit_value):
    update_value(file_path, header, 0, limit_value)

def update_high_limit(file_path, header, limit_value):
    update_value(file_path, header, 1, limit_value)