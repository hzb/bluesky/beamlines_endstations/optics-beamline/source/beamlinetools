import pandas as pd
from bluesky.plan_stubs import mvr

class MotorSelector:
    
    def __init__(self, RE, device_name, motor_names:list, motors:list, csv_path, reset_axis=False, additional_print_statement='') -> None:
        self.RE = RE
        self.csv_path = csv_path
        self.motor_names = motor_names
        self.motors = motors
        self.device_name = device_name
        self.reset_axis = reset_axis
        self.additional_print_statement = additional_print_statement
        
    def selector(self, target_device_index=None):
    
        # Read the CSV file
        data_file = pd.read_csv(self.csv_path, index_col=False)

        # Print the current {self.device_name}
        current_device_index = self.RE.md[f'current_{self.device_name}']
        print(f'Current {self.device_name}: {current_device_index}')

        # If no target_device_index specified, ask
        if target_device_index is None:
            # self.print_info_table(data_file)
            print(data_file)
            target_device_index = self.get_target_device_index_input(data_file)

        # update the Run Engine md
        self.RE.md[f'current_{self.device_name}'] = target_device_index

        # Instatiate position lists
        movements = []
        current_position = []
        
        for index, motor_name in enumerate(self.motor_names):
            current = None
            target = None

            movements.append(self.motors[index])
            if self.reset_axis:
                current_position.append(self.motors[index].readback.get())
            # Get the current {self.device_name}'s position
            current = data_file[data_file[f'{self.device_name}'] == current_device_index][[motor_name]].values[0]
        
            # Get the target_device_index {self.device_name}'s position
            target = data_file[data_file[f'{self.device_name}'] == target_device_index][[motor_name]].values[0]

            movements.append(float(target) - float(current))
        

        # Calculate the movement required
        print(f'Changing to {self.device_name}: {target_device_index}')

        # unpack movements and perform plan
        self.RE(mvr(*movements))
        
        if self.additional_print_statement is not  None:    
            print(f'\n{self.additional_print_statement}') 
            
        # reset the value of the motors to the current positions 
        if self.reset_axis:
            for index, motor_name in enumerate(self.motor_names):
                self.motors[index].set_current_position(current_position[index])  
                print(f'{motor_name}: {self.motors[index].readback.get()}')          

               
    
    def get_target_device_index_input(self, data_file):
        while True:
            try:
                target_device_index = int(input("Enter the target_device_index {self.device_name} number: "))
                if target_device_index < 0 or target_device_index > len(data_file)-1:
                    raise ValueError
                return target_device_index
            except ValueError:
                print(f"Invalid input. Please enter a number between 1 and {len(data_file)}.")

