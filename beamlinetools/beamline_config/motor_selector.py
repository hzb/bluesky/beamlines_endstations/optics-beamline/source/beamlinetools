from beamlinetools.utils.selector import MotorSelector
from .base import RE
from .beamline import *
# from beamlinetools.utils.pbar_bessy import ProgressBarManager 
# RE.waiting_hook = ProgressBarManager()

print('\n\nLOADING tools.py')

try:
    c = MotorSelector(RE, "Detector", ['x [mm]', 'y [deg]'], 
                    [r.det1, r.twt], 
                    '/opt/bluesky/beamlinetools/beamlinetools/utils/detector_values.csv', 
                    reset_axis=True, 
                    additional_print_statement='New Io abs. coordinates')
    Detector = c.selector
    print('Detector function activated')
except Exception as e:
    print(f'Detector function not active: {e}')

try:
    s1 = MotorSelector(RE, "Slit1", ['Position [mm]'], [fsu.slt1], '/opt/bluesky/beamlinetools/beamlinetools/utils/slit1.csv')
    Slit1 = s1.selector
    print('Slit1 function activated')
except Exception as e:
    print(f'Slit1 function not active: {e}')

try:
    s2 = MotorSelector(RE, "Slit2", ['Position [mm]'], [fsu.slt2], '/opt/bluesky/beamlinetools/beamlinetools/utils/slit2.csv')
    Slit2 = s2.selector
    print('Slit2 function activated')
except Exception as e:
    print(f'Slit2 function not active: {e}')

try:
    f1 = MotorSelector(RE, "Filter1", ['Position [mm]'], [fsu.flt1], '/opt/bluesky/beamlinetools/beamlinetools/utils/flt1.csv')
    Filter1 = f1.selector
    print('Filter1 function activated')
except Exception as e:
    print(f'Filter1 function not active: {e}')

try:
    f2 = MotorSelector(RE, "Filter2", ['Position [mm]'], [fsu.flt2], '/opt/bluesky/beamlinetools/beamlinetools/utils/flt2.csv')
    Filter2 = f2.selector
    print('Filter2 function activated')
except Exception as e:
    print(f'Filter2 function not active: {e}')