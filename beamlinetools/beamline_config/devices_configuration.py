devices_dict = {
    "accelerator": {
        'axes' : {
                'next_injection': ['time_next_injection', 'time_last_injection'],

        },
        'device_type' : 'motor',
        'headers' : ['next_injection', 'last_injection']
    },
    
    "valves": {
        'axes' : {
                'bs': ['status'],
                'v3': ['readback'],
                'v4': ['readback'],
                'v5': ['readback'],
                'v6': ['readback'],
                'v7': ['readback'],
                'v8': ['readback'],
                'v9': ['readback'],
                'v10': ['readback'],
                'v11': ['readback'],
                'v13': ['readback'],
        },
        'device_type' : 'motor',
        'headers' : ['readback']
    },
    "wau1": {
        'axes' : {
                'wau1.top': ['readback'],
                'wau1.bottom': ['readback'],
                'wau1.left': ['readback'],
                'wau1.right': ['readback'],
        },
        'device_type' : 'motor',
        'headers' : ['readback']
    },
    "m1": {
        'axes' : {
                'm1.tx': ['readback'],
                'm1.ty': ['readback'],
                'm1.rx': ['readback'],
                'm1.ry': ['readback'],
                'm1.rz': ['readback'],
        },
        'device_type' : 'motor',
        'headers' : ['readback']
    },
    "pgm": {
        'axes' : {
                'pgm.en': ['readback'],
                'pgm.cff': [''],
                'pgm.grating': [''],
                'pgm.beta': ['readback'],
                'pgm.theta': ['readback'],
                'pgm.alpha': ['readback'],
                'pgm.slit': ['readback'],
        },
        'device_type' : 'motor',
        'headers' : ['readback']
    },
    "hios": {
        'axes' : {
                'hios.htr': ['readback'],
                'hios.hm1': ['readback'],
                'hios.hm2': ['readback'],
                'hios.hm2_absolute': ['readback'],
        },
        'device_type' : 'motor',
        'headers' : ['readback']
    },
    "fsu": {
        'axes' : {
                'fsu.flt1': ['user_readback', 'user_offset', 'high_limit_switch', 'low_limit_switch'],
                'fsu.flt2': ['user_readback', 'user_offset', 'high_limit_switch', 'low_limit_switch'],
                'fsu.slt1': ['user_readback', 'user_offset', 'high_limit_switch', 'low_limit_switch'],
                'fsu.slt2': ['user_readback', 'user_offset', 'high_limit_switch', 'low_limit_switch'],
        },
        'device_type' : 'motor',
        'headers' : ['readback', 'offset', 'high_limit', 'low_limit']
    },
    "reflectometer" : {
        'axes' :{
                'r.phi': ['readback', 'abs_readback', 'offset'],
                'r.tha': ['readback', 'abs_readback', 'offset'],
                'r.twt': ['readback', 'abs_readback', 'offset'],
                'r.tx': ['readback', 'abs_readback', 'offset'],
                'r.ty': ['readback', 'abs_readback', 'offset'],
                'r.tz': ['readback', 'abs_readback', 'offset'],
                'r.rx': ['readback', 'abs_readback', 'offset'],
                'r.ry': ['readback', 'abs_readback', 'offset'],
                'r.rz': ['readback', 'abs_readback', 'offset'],
                'r.det1': ['readback', 'abs_readback', 'offset'],
                'r.det2': ['readback', 'abs_readback', 'offset'],
        },
        'device_type' : 'motor',
        'headers' : ['readback', 'abs_readback', 'offset']
    },
    "keithleys" : {
        'axes' :{
                'kth1': ['readback', 'nplc', 'int_time'],
                'kth2': ['readback', 'nplc', 'int_time'],
                'kth3': ['readback', 'nplc', 'int_time'],
                'kth4': ['readback', 'nplc', 'int_time'],
        },
        'device_type' : 'detector',
        'headers' : ['readback', 'nplc', 'integration_time']
    },
}