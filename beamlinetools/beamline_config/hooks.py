import numpy as np
from .base import *
from .beamline import *

from bluesky_hooks.hooks import MotionHook, MotionHooksPreprocessor
import bluesky.plan_stubs as bps


print('\n\nLOADING hooks.py')
def find_corresponding_value(autohios_table, en):
    table = np.loadtxt(autohios_table, skiprows=2)
    # Extract the first column (arr) from the table
    energies = table[:, 0]
    # Find the values in the arr that are strictly lower than "en"
    lower_values = energies[energies < en]

    if len(lower_values) > 0:
        # Find the maximum value among the strictly lower values
        closest_lower_value_index = np.argmax(lower_values)
        return closest_lower_value_index
    else:
        raise ValueError(f"There is no value strictly lower than {en} eV, in the {autohios_table} table.")

 
autohios_active  = False
autohios_table_folder = '/opt/bluesky/beamlinetools/beamlinetools/hios'
authios_tables = ['hios_1.txt', 'hios_2.txt', 'hios_3.txt']


def _get_hios_position_from_table(en):
    #extract the poisitions from the tables that andrey will provide
    closest_en_index = find_corresponding_value(autohios_table, en)
    table = np.loadtxt(autohios_table, skiprows=2)
    hm1_pos = table[closest_en_index,3]
    hm2_pos = table[closest_en_index,2]
    htr_pos = table[closest_en_index,1]
    return hm1_pos, hm2_pos, htr_pos

def autohios(number=None):
    number = int(number)
    if number not in [0,1,2,3]:
        raise ValueError(f'Use 0 to deactivate autohios, 1,2 or 3 to activate hios with tables 1,2, or 3')
    if number != 0:
        global autohios_active
        autohios_active = True
        global autohios_table
        autohios_table   = os.path.join(autohios_table_folder, f'hios_{str(number)}.txt')
        print('Autohios is now active')
        return
    
    elif number == 0:
        autohios_active = False
        print('Autohios is now deactivated')

def hios_hook(val):
        if autohios_active:
            hm1_pos, hm2_pos, htr_pos = _get_hios_position_from_table(val)
            yield from bps.mov(hios.htr, htr_pos)
            yield from bps.mov(hios.hm1, hm1_pos, hios.hm2pseudo.hm2, hm2_pos)
            # yield from bps.mov(hios.hm2pseudo.hm2, hm2_pos)

if 'pgm' in devices_dictionary.keys():
    hios_hook  = MotionHook(pgm.en,hios_hook,[])
    mh = MotionHooksPreprocessor()
    RE.preprocessors.append(mh)
    mh.hooks = [hios_hook]
    print("autohios is instantiated")
else:
    print('autohios is not available')