# Define here data export callbacks
from .base import RE
from beamlinetools.data_management.data_structure import BessyDataStructure

bds = BessyDataStructure(RE)