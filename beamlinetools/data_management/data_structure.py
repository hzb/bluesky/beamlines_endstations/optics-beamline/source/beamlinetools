import time
import os
import socket
from datetime import datetime

from event_model import RunRouter
from beamlinetools.callbacks.file_exporter import CSVCallback
from beamlinetools.data_management.specfile_management import spec_factory, new_spec_file


class BessyDataStructure:
    """
    This class is used to create the data structure needed at Bessy.
    All the data is saved under ~/bluesky/data

    The class should be initialized in a startup file like this:
        from beamlinetools.data_management.data_structure import BessyDataStructure
        bds = BessyDataStructure()
    
    Then from the IPython session one can create a new user folder:
        bds.change_user('<user_name>')
    
    For different parts of the sample, a new folder with a new specfile
    can be created with:
        bds.change_sample('<sample_name>')   

    To switch to the commissioning mode, use:
        bds.commissioning()
    In this case the files are saved in the YYYY_commissioning folder.

    The scan_id can be set to an arbitrary value:
        bds.set_scan_id_to(value)

    The scan_id will be reset to zero at the beginning of the 
    year automatically.

    This is the folder structure that will be created:
    ├── data
    │   ├── 2024_commissioning
    │   │   ├── csv
    │   │   │   ├── 00001_meta.json
    │   │   │   ├── 00001_primary.csv
    │   │   │   ├── 00001_baseline.csv
    │   │   └── commissioning_2024.spec
    │   ├── new_user
    │   │   └── sample1
    │   │   │   ├── csv
    │   │   │   │   ├── 00001_meta.json
    │   │   │   │   ├── 00001_primary.csv
    │   │   │   │   └── 00001_baseline.csv
    │   │   │   └── sample1.spec
    │   │   └── sample2
    │   │       ├── csv
    │   │       │   ├── 00001_meta.json
    │   │       │   ├── 00001_primary.csv
    │   │       │   └── 00001_baseline.csv
    │   │       └── sample2.spec
    """

    def __init__(self, RE):
        """
        Initialize the BessyDataStructure class.
        
        Args:
            RE: RunEngine instance
        """
        self.RE = RE
        self._user_folder = None  # Defined by newdata or read from RE.md
        self._bluesky_data = "/opt/bluesky/data"
        self._user_folder = None
        self.csv = None
        self._sample_folder = None
        
        self._commissioning_folder = str(datetime.now().year) + "_commissioning"
        self._commissioning_abs_path = os.path.join(self._bluesky_data, self._commissioning_folder)
        
        self._reset_scan_id_if_new_year()
        self._create_commissioning_folder()
        self._load_existing_keys()
        self._init_and_subscribe_to_csv_export()
        self._init_and_subscribe_to_csv_export_commissioning()
        self._set_csv_export_folder()
        self._set_csv_export_folder_commissioning()
        self._init_and_subscribe_spec_factory()
        self._init_and_subscribe_spec_factory_commissioning()
        self.print_info()
    
    def _reset_scan_id_if_new_year(self):
        # Get the current year in YYYY format
        current_year = datetime.now().year
        
        # Check if 'current_year' key exists in the dictionary 'RE.md'
        if 'current_year' not in self.RE.md:
            # If not, add the current year
            self.RE.md['current_year'] = current_year
        else:
            # If it exists, check if the current year is different from the stored year
            if self.RE.md['current_year'] != current_year:
                # If they differ, reset the scan ID to zero and update the year
                self.set_scan_id_to(0)
                self.RE.md['current_year'] = current_year

    def print_info(self):
        """Print information about the data export location."""
        if self._user_folder is not None and self._sample_folder is not None:
            data_folder = os.path.join("~/bluesky/", self._user_folder, self._sample_folder)
            msg = f"The data will be exported in \n{data_folder}"
        else:
            data_folder = os.path.join("~/bluesky/", self._commissioning_folder)
            msg = f"The data will be exported in \n{data_folder}"
        print()
        print()
        print("##################################################################")
        print(msg)
        print("##################################################################")
        print()
        print()

    def _init_and_subscribe_to_csv_export(self):
        """Initialize and subscribe to CSV export."""
        self.csv = CSVCallback(file_path=self._bluesky_data)
        self.RE.subscribe(self.csv.receiver)

    def _init_and_subscribe_to_csv_export_commissioning(self):
        """Initialize and subscribe to CSV export for commissioning."""
        self.csv_commissioning = CSVCallback(file_path=self._bluesky_data)
        self.RE.subscribe(self.csv_commissioning.receiver)

    def _set_csv_export_folder(self):
        """Set the folder for CSV export."""
        if self._user_folder is None or self._sample_folder is None:
            self._user_folder = ''
            self._sample_folder = self._commissioning_folder
        self.csv.change_user(os.path.join(self._bluesky_data, self._user_folder, self._sample_folder))

    def _set_csv_export_folder_commissioning(self):
        """Set the folder for CSV export during commissioning."""
        self.csv_commissioning.change_user(self._commissioning_folder)

    def _init_and_subscribe_spec_factory(self):
        """Initialize and subscribe to spec factory."""
        spec_factory.file_prefix = self._sample_folder
        spec_factory.directory = os.path.join(self._bluesky_data, self._user_folder, self._sample_folder)

        rr = RunRouter([spec_factory])
        self.RE.subscribe(rr)
    
    def _init_and_subscribe_spec_factory_commissioning(self):
        """Initialize and subscribe to spec factory for commissioning."""
        year = time.strftime('%Y')
        spec_factory.file_prefix = 'commissioning_' + year
        spec_factory.directory = self._commissioning_abs_path
        spec_factory.directory = os.path.join(self._bluesky_data, self._user_folder, self._sample_folder)

        rr_com = RunRouter([spec_factory])
        self.RE.subscribe(rr_com)
    
    def commissioning(self):
        """Set up CSV export folder and specfile for commissioning."""
        self.csv.change_user(self._commissioning_folder)
        
        year = time.strftime('%Y')
        spec_factory.file_prefix = 'commissioning_' + year
        spec_factory.directory = self._commissioning_abs_path
        
    def _create_commissioning_folder(self):
        """Create the commissioning folder if it doesn't exist."""
        if not os.path.exists(self._commissioning_abs_path):
            os.makedirs(self._commissioning_abs_path)
        
    def _load_existing_keys(self):
        """Load existing keys from RunEngine metadata."""
        try:
            self._user_folder = self.RE.md["user_folder"]
            self._sample_dir = os.path.join(self._bluesky_data, self._user_folder)
        except KeyError:
            self._user_folder = None
        
        try:
            self._sample_folder = self.RE.md["sample_folder"]
        except KeyError:
            self._sample_folder = None

        try:
            self._newdata_dir = self.RE.md['exported_data_dir']
        except KeyError:
            self._newdata_dir = None

    def change_user(self, user_folder):
        """
        Change the user group name. 
        
        A folder called user_folder will be created: 
        ~/bluesky/data/user_folder 

        Args:
            user_folder (str): Name of the user group.
        """
        self.RE.md["user_folder"] = user_folder
        self._user_folder = user_folder
        self._sample_dir = os.path.join(self._bluesky_data, self._user_folder)
        
        if not os.path.exists(self._sample_dir):
            os.makedirs(self._sample_dir)
        
    def change_sample(self, sample_folder):
        """
        Change the sample description and create a new folder.

        Args:
            sample_folder (str): Description of the sample.
        """
        self._sample_folder = sample_folder   
        self._newdata_dir = os.path.join(self._sample_dir, self._sample_folder)
        
        print(f'self._sample_folder {self._sample_folder}')
        print(f'self._commissioning_folder, {self._commissioning_folder}')
        if self._user_folder == self._commissioning_folder:
            print(f'Switched to folder {self._newdata_dir}')

        elif not os.path.exists(self._newdata_dir):
            os.makedirs(self._newdata_dir)
            print(f'Created new folder {self._newdata_dir}')
        else:
            raise ValueError(f'Existing folder detected {self._newdata_dir}. This should not happen, ask beamline scientist what to do')
        
        self.csv.change_user(self._newdata_dir)  # Change folder for CSV export
        spec_factory.file_prefix = self._sample_folder  # Change specfile name
        spec_factory.directory = self._newdata_dir

        # Update RE.md with new values
        self.RE.md['sample_folder'] = self._sample_folder
        self.RE.md['specfile'] = os.path.join(self._newdata_dir, self._sample_folder)
        self.RE.md['hostname'] = socket.gethostname()
        self.RE.md['exported_data_dir'] = self._newdata_dir
        
        if 'scan_id' in self.RE.md.keys():
            print(f'Next scan is number: {self.RE.md["scan_id"] + 1}')
        else:
            print(f'Next scan is number: 0')

    def commissioning(self):
        '''Export the data to the commissioning folder of the current year.
        '''
        self.change_user(self._commissioning_folder)
        self.change_sample('')

    def set_scan_id_to(self, value):
        '''Set the scan-id to a non-negative integer value.'''
        
        if not isinstance(value, int) or value < 0:
            raise ValueError("The scan ID must be a non-negative integer")
            
        self.RE.md['scan_id'] = value


    
